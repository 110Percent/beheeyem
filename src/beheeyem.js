/*
 * Beheeyem
 */

const { ShardingManager } = require('discord.js');
const { join } = require('path');
const config = require('../config');
const logger = require('./core/logger');
require('./cache');

// Initialize the bot
const manager = new ShardingManager(join(__dirname, './core/discord-client.js'), { token: config.client.token });
manager.spawn();

manager.on('launch', shard => {
  logger.info(`Launched Shard #${shard.id}`);
});

const guildCounts = [];
manager.on('message', (shard, msg) => {
  if (msg.type === 'guildCount') {
    guildCounts.push(msg.count);
    if (guildCounts.length === manager.totalShards) {
      logger.info('Beheeyem is ready!');
      const count = guildCounts.reduce((a, b) => a + b, 0);
      logger.info(`Running on ${manager.totalShards} shards in a total of ${count} guilds.`);
      manager.shards.forEach(i => {
        i.eval(`this.user.setPresence({ game: { name: "${count} guilds", type: "WATCHING" } })`);
      });
    }
  }
});

const cycleStatus = async () => {
  logger.info('Changing presence message.');
  const selection = Math.floor(Math.random() * 5);
  if (selection === 0) {
    const count = (await manager.fetchClientValues('guilds.size')).reduce((a, b) => a + b);
    manager.shards.forEach(i => {
      i.eval(`this.user.setPresence({ game: { name: "${count} guilds", type: "WATCHING" } })`);
    });
  } else if (selection === 1) {
    const shardMembers = (
      await manager.broadcastEval('this.guilds.reduce((prev, guild) => prev + guild.memberCount, 0)')
    ).reduce((a, b) => a + b);
    manager.shards.forEach(i => {
      i.eval(`this.user.setPresence({ game: { name: "${shardMembers} users", type: "WATCHING" } })`);
    });
  } else if (selection === 2) {
    const channels = (
      await manager.broadcastEval('this.guilds.reduce((prev, guild) => prev + guild.channels.size, 0)')
    ).reduce((a, b) => a + b);
    manager.shards.forEach(i => {
      i.eval(`this.user.setPresence({ game: { name: "${channels} channels", type: "WATCHING" } })`);
    });
  } else if (selection === 3) {
    manager.shards.forEach(i => {
      i.eval('this.user.setPresence({ game: { name: "Use b.invite to invite me to your server!" } })');
    });
  } else if (selection === 4) {
    manager.shards.forEach(i => {
      i.eval('this.user.setPresence({ game: { name: "Use b.help for a list of commands!" } })');
    });
  }
};

// Cycle status every 10 minutes
setInterval(cycleStatus, 600000);

require('./core/settings');

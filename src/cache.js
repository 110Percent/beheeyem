const Fuse = require('fuse.js');
const express = require('express');

const sets = {
  abilities: require('./external/abilities'),
  formats: require('./external/formats'),
  items: require('./external/items'),
  moves: require('./external/moves'),
  pokedex: require('./external/dex')
};

class PokeFuse extends Fuse {
  constructor(values, keys) {
    super(values, {
      shouldSort: true,
      tokenize: true,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys
    });
  }
}

const fuses = {
  abilities: new PokeFuse(Object.values(sets.abilities), ['name', 'id', 'desc']),
  items: new PokeFuse(Object.values(sets.items), ['name', 'num', 'desc']),
  moves: new PokeFuse(Object.values(sets.moves), [
    'name',
    'num',
    'desc',
    'type',
    'category'
  ]),
  pokedex: new PokeFuse(Object.values(sets.pokedex), ['name', 'num'])
};

const evoFormat = (name) => {
  let newName = name;
  if (/-alola/i.test(newName)) {
    newName = `Alolan ${newName.replace(/-alola/i, '')}`;
  }
  return newName;
};

const evoString = (entry) => {
  // Create base strings for construction
  const eString = `**${entry.name}**`;
  let preString = '';
  let postString = '';

  // If the pokemon has a prevolution, set it as the prevo string
  if (entry.prevo || entry.basename) {
    const prevo =
      sets.pokedex[entry.prevo] || sets.pokedex[entry.basename.toLowerCase()];
    preString = evoFormat(prevo.name) + ' > ';

    // If the prevolution has a prevolution, add that to the beginning of the string
    if (prevo.prevo) {
      const pPrevo = sets.pokedex[prevo.prevo];
      preString = evoFormat(pPrevo.name) + ' > ' + preString;

      // This only applies to mega evolutions, since no evo chain exceeds 3 in length
      if (pPrevo.prevo) {
        preString = evoFormat(sets.pokedex[pPrevo.prevo].name) + ' > ' + preString;
      }
    }
  }

  // If the Pokémon has any evolutions, add them to the string
  if (entry.evos) {
    postString =
      ' > ' + entry.evos.map((p) => evoFormat(sets.pokedex[p].name)).join(', ');

    let furtherEvo = false;
    for (let i = 0; i < entry.evos.length; i++) {
      if (sets.pokedex[entry.evos[i]].evos) {
        furtherEvo = true;
        break;
      }
    }

    if (furtherEvo) {
      postString += ' > ';
      postString += entry.evos
        .map((c) => sets.pokedex[c].evos.map((d) => evoFormat(sets.pokedex[d].name)))
        .join(', ');
    }
  }

  return preString + eString + postString;
};

const server = express();

server.get('/search/:set', (req, res) => {
  if (!fuses[req.params.set]) return;
  let results;
  if (req.query.q === 'random') {
    results = [
      sets[req.params.set][
        Object.keys(sets[req.params.set])[
          Math.floor(Math.random() * Object.keys(sets[req.params.set]).length)
        ]
      ]
    ];
  } else {
    results = fuses[req.params.set].search(req.query.q);
  }
  if (results.length > 0) {
    const result = results[0];
    result.identifier = Object.keys(sets[req.params.set])[
      Object.values(sets[req.params.set]).indexOf(result)
    ];
    if (req.params.set === 'pokedex') {
      result.evoString = evoString(result);
    }
    res.send(result);
  } else {
    res.sendStatus(404);
  }
});

server.get('/get/:set', (req, res) => {
  if (!sets[req.params.set]) return;
  res.send(sets[req.params.set][req.query.q]);
});

server.listen(6060);

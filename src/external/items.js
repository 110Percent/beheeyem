/*
 * items - This file manages the Item db for use with the item command.
 */

// Require modules
const fs = require('fs');
const path = require('path');
const rp = require('request-promise');
const logger = require('../core/logger');
const reload = require('require-reload')(require);

// URL to fetch data from
const uri =
  'https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/items.js';

// Declare the initial db
let items = {};
try {
  items = require('../../data/items');
} catch (err) {
  items = {};
}

// Function to fetch the Item DB and download it to the data folder
const cacheFile = async () => {
  const data = await rp({ uri, method: 'GET' });
  fs.writeFileSync(path.join(__dirname, '../../data/items.js'), data);
  try {
    items = reload('../../data/items');
    logger.info('Cached Item DB!');
  } catch (e) {
    cacheFile();
  }
};

// Cache the data on start
setTimeout(cacheFile, 60000)

// Cache the data every 15 minutes (900 000 ms)
setInterval(cacheFile, 900000);

module.exports = items.BattleItems;

/*
 * dex - This file manages the Ability db for use with the ability command.
 */

// Require modules
const fs = require('fs');
const path = require('path');
const rp = require('request-promise');
const logger = require('../core/logger');
const reload = require('require-reload')(require);

// URL to fetch data from
const uri =
  'https://cdn.jsdelivr.net/gh/Zarel/Pokemon-Showdown@master/data/abilities.js';

// Declare the initial db
let abilities = {};
try {
  abilities = require('../../data/abilities');
} catch (err) {
  abilities = {};
}

// Function to fetch the Ability DB and download it to the data folder
const cacheFile = async () => {
  const data = await rp({ uri, method: 'GET' });
  fs.writeFileSync(path.join(__dirname, '../../data/abilities.js'), data);
  try {
    abilities = reload('../../data/abilities');
    logger.info('Cached Ability DB!');
  } catch (e) {
    cacheFile();
  }
};

// Cache the data on start
setTimeout(cacheFile, 60000)

// Cache the data every 15 minutes (900 000 ms)
setInterval(cacheFile, 900000);

module.exports = abilities.BattleAbilities;

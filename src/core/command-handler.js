/*
 * Main file to handle command parsing
 */

// Require modules
const logger = require('./logger');
const config = require('../../config');
const cmds = require('../commands');
const settings = require('./settings');

// Get an object containing the bot's commands
const commands = cmds.commands;

// Function to handle messages
const handle = ({ msg, client }) => {
  let length;
  if (msg.content.startsWith(config.bot.prefix)) {
    length = config.bot.prefix.length;
  } else {
    length = settings.get(msg.guild.id, 'customPrefix').length;
  }

  const content = msg.content.substring(length);

  // Create an array of the message contents split by spaces
  const cSplit = content.split(' ');

  // Get the name of the attempted command
  const attCmd = cSplit[0];

  // If the attempted command doesn't exist, ignore the rest of this function
  if (!commands[attCmd]) return;

  // Get a string containing the command's arguments
  let args = [...cSplit];
  args.splice(0, 1);
  args = args.join(' ');

  // Execute the command, feeding the message object and the command arguments
  try {
    commands[attCmd].execute({ msg, args, commands, client });
  } catch (err) {
    logger.error(`Error using command ${attCmd}: ${err}`);
  }
};

module.exports = {
  handle: handle
};

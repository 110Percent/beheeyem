/*
 * This file contains the module used to easily pull sprites from Showdown.
 */

const rp = require('request-promise');

const urls = [
  'ani',
  'gen5',
  'gen5ani',
  'gen4',
  'gen4dp',
  'gen4dp-2',
  'gen3',
  'gen3-2',
  'gen3frlg',
  'gen2',
  'gen2g',
  'gen2s',
  'gen1',
  'gen1rb',
  'gen1rg',
  'digimon/sprites/digimon'
];
const gifRegex = new RegExp('>[a-zA-Z0-9\\-]+.(gif|png)<', 'gm');
const lists = {};
const genAliases = {
  bw: 'gen5',
  bwani: 'gen5ani',
  'bw-ani': 'gen5ani',
  hgss: 'gen4',
  dp: 'gen4dp',
  rs: 'gen3',
  frlg: 'gen3frlg',
  crystal: 'gen2',
  gold: 'gen2g',
  silver: 'gen2s',
  yellow: 'gen1',
  rb: 'gen1rb',
  rg: 'gen1rg'
};

(async () => {
  for (let i = 0; i < urls.length; i++) {
    const category = urls[i];
    const html = await rp.get(`https://play.pokemonshowdown.com/sprites/${urls[i]}`)
    lists[category] = html.match(gifRegex).map(c => c.substr(1, c.length - 2));
  }
})();

const handle = async (content) => {
  if (!content) return;

  const flags = {};
  let presetList = null;
  content = content.toLowerCase();

  const split = content.split(' ');
  if (urls.indexOf(split[split.length - 1]) > -1) {
    presetList = split[split.length - 1];
    split.splice(split.length - 1, 1);
    content = split.join(' ');
  } else if (Object.keys(genAliases).indexOf(split[split.length - 1]) > -1) {
    presetList = genAliases[split[split.length - 1]];
    split.splice(split.length - 1, 1);
    content = split.join(' ');
  }

  if (content.indexOf('shiny ') > -1) {
    content = content.replace('shiny ', '');
    flags.shiny = true;
  }

  if (content.indexOf('mega ') > -1) {
    content = content.replace('mega ', '');
    flags.mega = true;
  }

  if (content.indexOf('alolan ') > -1) {
    content = content.replace('alolan ', '');
    flags.alolan = true;
  }

  if (content.indexOf('gigantamax ') > -1) {
    content = content.replace('gigantamax ', '');
    flags.gmax = true;
  }

  if (content.indexOf('gigamax ') > -1) {
    content = content.replace('gigamax ', '');
    flags.gmax = true;
  }

  if (content.indexOf('gmax ') > -1) {
    content = content.replace('gmax ', '');
    flags.gmax = true;
  }

  if (content.endsWith('gmax')) {
    content = content.substr(0, content.length - 4);
    flags.gmax = true;
  }

  if (content === 'greninjaash' || content === 'ash greninja') {
    content = 'greninja-ash';
  }

  let search = content;

  if (flags.mega) {
    search += '-mega';
  }
  if (flags.alolan) {
    search += '-alola';
  }
  if (flags.gmax) {
    search += '-gmax';
  }

  const reg = search.match(/mega[a-z]$/);
  if (reg) {
    search = search.replace(reg[0], `-${reg[0]}`);
  }
  var returnUrl;
  if (presetList) {
    returnUrl = checkUrl(presetList, lists[presetList], search, flags);
  } else {
    for (const list in lists) {
      returnUrl = checkUrl(list, lists[list], search, flags);
      if (returnUrl) {
        // Since Showdown doesn't have animated Gen 8 sprites, check Smogon
        if (list === 'gen5' && !flags.shiny) {
          const check = await rp.get({
            uri: `https://www.smogon.com/dex/media/sprites/xy/${search}.gif`,
            resolveWithFullResponse: true
          });
          if (check.statusCode === 200) {
            returnUrl = `https://www.smogon.com/dex/media/sprites/xy/${search}.gif`;
          }
        }
        break;
      }
    }
  }
  return returnUrl;
}

const checkUrl = (name, list, search, flags) => {
  const extension = list[0].split('.')[list[0].split('.').length - 1];
  const index = list.indexOf(`${search}.${extension}`);
  if (index > -1) {
    return `https://play.pokemonshowdown.com/sprites/${name}${flags.shiny ? '-shiny' : ''}/${search}.${extension}`;
  } else {
    return null;
  }
}

module.exports = handle;

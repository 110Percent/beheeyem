/*
 * invite
 *
 * This command posts an invite link for Beheeyem.
 *
 */

// Require modules
const logger = require('../core/logger');
const { RichEmbed } = require('discord.js');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const invite = new Command();

// Set the display attributes for the command
invite.description = 'Get a link to invite the bot to your server.';
invite.usage = 'invite';

// Set the command's action
invite.execute = async ({ msg, client }) => {
  const embed = new RichEmbed();
  embed
    .setTitle('Beheeyem')
    .setColor('#C2926E')
    .setThumbnail(client.user.displayAvatarURL)
    .setDescription(
      'Thank you for using Beheeyem! Feel free to invite me to your server using [this link]' +
        `(https://discordapp.com/oauth2/authorize?client_id=${client.user.id}&scope=bot&permissions=67488832).`
    );

  msg.channel.send({ embed });
  logger.info(`Sent a server invite guild ${msg.guild.name}`);
};

module.exports = invite;

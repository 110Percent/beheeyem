/*
 * move
 *
 * This command returns information on a move.
 *
 */

// Require modules
const logger = require('../core/logger');
const Discord = require('discord.js');
const request = require('request-promise');

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const moveCommand = new Command();

// Set the display attributes for the command
moveCommand.description = 'Show info on a move';
moveCommand.usage = 'move <name>';

const baseURL = 'https://cdn.jsdelivr.net/gh/jalyna/oakdex-pokedex/data/move';

const terminology = {
  targets: {
    normal: 'One Enemy',
    adjacentAlly: 'One Ally',
    self: 'Self',
    allAdjacent: 'All Pokémon',
    allySide: 'Ally Team',
    allyTeam: 'Ally Team',
    foeSide: 'Opponent Team'
  },
  categories: {
    Physical: '<:physical:603419123891961866>',
    Special: '<:special:603419125036875776>',
    Status: '<:status:603419123837566976>',
    Viable: '<:viable:603413577621176333>'
  }
};

// Set the command's action
moveCommand.execute = async ({ msg, args }) => {
  const baseEmbed = new Discord.RichEmbed();

  request
    .get(`http://localhost:6060/search/moves?q=${encodeURIComponent(args)}`)
    .then(async (m) => {
      const move = JSON.parse(m);
      const cleanName = move.name.toLowerCase().replace(/[-, ]/g, '_');
      let flavourText;
      try {
        flavourText = await request.get({
          url: `${baseURL}/${cleanName}.json`,
          json: true
        });
        flavourText = flavourText.pokedex_entries;
        flavourText = Object.keys(flavourText).reduce((r, e) => {
          if (flavourText[e].en !== 'Unknown') r[e] = flavourText[e];
          return r;
        }, {});

        baseEmbed.setDescription(
          '\u200b\n*' +
            Object.values(flavourText)[Object.values(flavourText).length - 1].en +
            '*\n\n' +
            (move.desc || move.shortDesc)
        );
      } catch (err) {}

      // Initialize embed
      baseEmbed.setTitle(
        `${terminology.categories[move.category]} ${move.name} ${
          move.isViable ? terminology.categories.Viable : ''
        }`
      );
      baseEmbed.addField('Type', move.type, true).addField('PP', move.pp, true);

      if (move.basePower > 0) {
        baseEmbed.addField('Base Power', move.basePower, true);
      }

      baseEmbed
        .addField('Accuracy', move.accuracy === true ? '—' : move.accuracy + '%', true)
        .addField('Target', terminology.targets[move.target] || move.target, true)
        .addField('Priority', move.priority, true)
        .addField('External Resources', externalString(move.name));

      msg.channel.send({ embed: baseEmbed });
      logger.info(`Sent move ${move.name} to guild ${msg.guild.name}`);
    })
    .catch(() => {
      msg.channel.send('⚠ Move not found. Check your spelling and try again.');
    });
};

// Function to generate the "External Resources" part of the embed
const externalString = (name) => {
  // Empty array to store link strings
  const externals = [];

  // Generate links for each site
  externals.push(
    `[Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/${name.replace(
      / /g,
      '_'
    )}_(move\\))`
  );
  externals.push(
    `[Smogon](https://www.smogon.com/dex/ss/moves/${name
      .toLowerCase()
      .replace(/ /g, '_')})`
  );
  externals.push(
    `[PokémonDB](https://pokemondb.net/move/${name.toLowerCase().replace(/ /g, '-')})`
  );
  externals.push(
    `[Serebii](https://www.serebii.net/attackdex-ss/${name
      .toLowerCase()
      .replace(/ /g, '')}.shtml)`
  );

  // Return concatenated list
  return externals.join(' • ');
};

module.exports = moveCommand;

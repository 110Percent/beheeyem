/*
 * mango
 *
 * If you know, you know.
 *
 */

// Get the base command class and create a new command from it
const Command = require('../class/basecommand');
const mango = new Command();

mango.hidden = true;

// Set the command's action
mango.execute = ({ msg }) => {
  msg.channel.send('🥭');
};

module.exports = mango;

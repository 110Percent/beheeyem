/*
 * File to manage and collect commands
 */

// Require modules
const fs = require('fs');
const path = require('path');
const reload = require('require-reload')(require);
const logger = require('../core/logger');

let commands = {};

// Fetch a list of commands
const fetch = () => {
  // Get a list of files in the commands directory
  const cmdDir = fs.readdirSync(path.join(__dirname, './'));
  cmdDir.splice(cmdDir.indexOf('index.js'), 1);

  // Add each command to the database of commands
  for (let i = 0; i < cmdDir.length; i++) {
    // Grab the command's filename and require it, logging any errors in the process
    const filename = cmdDir[i];
    try {
      commands[filename.split('.')[0]] = reload(`./${filename}`);
      commands[filename.split('.')[0]].filename = filename;
      logger.info(`Successfully loaded command file ${filename}`);
    } catch (err) {
      logger.error(`Could not load command file ${filename}!\n${err}`);
    }
  }
}

// Refresh a single functions, or all of them
const refresh = (name) => {
  // If no command name is provided, refresh the entire command list
  if (!name) {
    commands = {};
    fetch();
  } else if (commands[name]) {
    commands[name] = reload(`./${name}`);
  }
}

module.exports = { fetch, refresh, commands };

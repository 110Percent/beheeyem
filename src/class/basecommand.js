/*
 * Base command class for every command to start from
 */

class Command {
    constructor() {
        this.description = '<no description>'
    }
}

module.exports = Command;
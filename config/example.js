/*
 * Example file for Beheeyem's config
 * This file is never used by the bot iself, but can be
 * used as a config reference
 */

const config = {
  client: {
    token: ''
  },
  bot: {
    prefix: 'b..',
    ownerIDs: ['']
  }
};

module.exports = config;
